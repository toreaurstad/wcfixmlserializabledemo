﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace CustomWcfSerialization.Common
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ICustomWcfSerializationService
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        List<Animal> GetAnimals(Animal composite);

        // TODO: Add your service operations here
    }
}
