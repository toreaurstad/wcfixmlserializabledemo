﻿
using CustomWcfSerialization.Client.CustomWcfSerializationService;
using CustomWcfSerialization.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomWcfSerialization.Host.Client
{
    class Program
    {

        static void Main(string[] args)
        {
           var favoriteAnimal = new Animal { Name = "Rex", IsBipedal = false };
           var proxy = new CustomWcfSerializationServiceClient();
           Animal[] result = proxy.GetAnimals(favoriteAnimal);

        }

    }
}
